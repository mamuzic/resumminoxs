# Install GSL
#https://coral.ise.lehigh.edu/jild13/2016/07/11/hello/
#wget ftp://ftp.gnu.org/gnu/gsl/gsl-1.16.tar.gz
source /cvmfs/cms.cern.ch/cmsset_default.sh
export SCRAM_ARCH=slc7_amd64_gcc630
export PATH=/cvmfs/cms.cern.ch/slc7_amd64_gcc630/external/cmake/3.7.0-cms/bin/:$PATH
export PATH=/cvmfs/cms.cern.ch/slc7_amd64_gcc630/external/lhapdf/6.2.1-cms/bin/:$PATH
export PATH=/cvmfs/cms.cern.ch/slc7_amd64_gcc630/external/boost/1.63.0-cms/include/:$PATH
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/cvmfs/cms.cern.ch/slc7_amd64_gcc630/external/gcc/6.3.0-cms/lib64/

gtar -zxvf gsl-1.16.tar.gz
cd gsl-1.16
./configure --prefix=$PWD
make
make check
make install

export PATH=/lhome/ific/m/mamuzic/bRPV/Resummino/gsl-1.16/include/:$PATH

# Install LHAPDF
# Download LHAPDF
# https://lhapdf.hepforge.org/
#wget http://www.hepforge.org/archive/lhapdf/LHAPDF-6.2.1.tar.gz
gtar -zxvf LHAPDF-6.2.1.tar.gz
cd LHAPDF-6.2.1
mkdir build
./configure --prefix=$PWD/build
make -j2 && make install
export PATH=/lhome/ific/m/mamuzic/bRPV/Resummino/LHAPDF-6.2.1/build/bin:$PATH
export LD_LIBRARY_PATH=/lhome/ific/m/mamuzic/bRPV/Resummino/LHAPDF-6.2.1/build/lib:$LD_LIBRARY_PATH

# Install Resummino
# Download Resummino
# https://resummino.hepforge.org/
# Download Resummino 2.0.1 including LoopTools-2.13
gtar -zxvf resummino-2.0.1-looptools.tar.bz2
cd resummino-2.0.1
mkdir build
mkdir install
cd build

export LHAPATH=/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AthAnalysisExternals/21.2.39/InstallArea/x86_64-slc6-gcc62-opt/share/LHAPDF:/cvmfs/atlas.cern.ch/repo/sw/Generators/lhapdfsets/current


mkdir helpers
ln -s /usr/lib64/libbz2.so.1.0.6 libbz2.so.1.0
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/lhome/ific/m/mamuzic/bRPV/Resummino/helpers/

cmake -DCMAKE_INSTALL_PREFIX=/lhome/ific/m/mamuzic/bRPV/Resummino/resummino-2.0.1/build -DCMAKE_PREFIX_PATH=/lhome/ific/m/mamuzic/bRPV/Resummino/gsl-1.16/lib -DLHAPDF=/lhome/ific/m/mamuzic/bRPV/Resummino/LHAPDF-6.2.1/build ..

make
cd src
cat CMakeFiles/resummino.dir/link.txt > tmp.sh
source tmp.sh
cd ../bin
export PATH=/lhome/ific/m/mamuzic/bRPV/Resummino/resummino-2.0.1/build/bin:$PATH
resummino ../../input/resummino.in --lo --o proba.txt

# cd RUN
# python condor.py
