#!/usr/bin/env python
"""Calculate Resummino cross-sections, i.e. submit jobs 
to Artemisa or lxplus condor systems, calculate cross-section and 
uncertainty for given LesHouches file and productions.
Example, run and merge Resummino jobs at Artemisa: 
python condor.py -i slha -p N1N2 -s PFD4LHC -r
python condor.py -i slha -p N1N2 -s PDF4LHC -m
"""
__author__ = "Judita Mamuzic"
__email__  = "judita.mamuzic@cern.ch"

import sys, getopt
import os, glob, json
import collections
import math
import pprint
from datetime import datetime

# Global variables
#Ncpj = 16 # No cores per job
Ncpj = 8 # No cores per job

# Productions, groupped
# HIGGSINO, STAU, EW, QCD
# TODO: Change to FS naming as in Prospino2.1 by Robin
allprods = {}
allprods["Higgsino"]        = ["N1N2","N2C1p","N2C1m","C1C1"]
allprods["HiggsinoInc"]     = ["N1N2","N1C1p","N1C1m","N2C1p","N2C1m","C1C1","N1N1","N2N2"]
allprods["HiggsinoIncHigh"] = ["N1N2","N1C1p","N1C1m","N2C1p","N2C1m"] # No C1C1, N1N1, N2N2
allprods["HiggsinoIncLow"]  = ["N1N1","N2N2","C1C1"]
allprods["C1C1"]            = ["C1C1"]
allprods["N1N1"]            = ["N1N1"]
allprods["NC"]              = ["N1N2","N2C1p","N2C1m"]


# Preapre a resummino.in file for SLHA, PDFSET, production, renorm and factorization scale
# Each run has unique resummino.in, command and outdir with all results
def runResumminoIn(slha, pdf_lo, pdfset_lo, pdf_nlo, pdfset_nlo, part1, part2, rscale, fscale, norder, cme, dirname): 
    pwd = os.getcwd()+"/"
    resname = dirname + "run/resummino_"+slha+"_"+str(part1)+"_"+str(part2)+"_"+pdf_lo+"_"+str(pdfset_lo)+"_"+pdf_nlo+"_"+str(pdfset_nlo)+"_"+str(rscale)+"_"+str(fscale)+".in"
    outname  = dirname + "results/result_"+slha+"_"+str(part1)+"_"+str(part2)+"_"+pdf_lo+"_"+str(pdfset_lo)+"_"+pdf_nlo+"_"+str(pdfset_nlo)+"_"+str(rscale)+"_"+str(fscale)+".json"
    fres = open(resname, "w")
    fres.write("""\n
# Input card for Resummino.
# This input card defines the hadron collider parameters and the process. The
# SUSY model is defined in a separate file in the SLHA file format.
# All energies and masses are in GeV.

# Collider parameters.
collider_type         = proton-proton  # proton-proton or proton-antiproton
center_of_mass_energy = %s

# Outgoing particles using the value of the PDG-scheme number.
# A negative particle number denotes the antiparticle. For the charginos the
# antiparticle is negatively charged, whereas the charged antislepton is
# positively charged.
# ---------------------------------------------------------------
# | ~e_L-     = 1000011 | ~e_R-   = 2000011 | ~nu_eL  = 1000012 |
# | ~mu_L-    = 1000013 | ~mu_R-  = 2000013 | ~nu_muL = 1000014 |
# | ~tau_1-   = 1000015 | ~tau_2- = 2000015 | ~nu_tau = 1000016 |
# ---------------------------------------------------------------
# | ~chi_10   = 1000022 | ~chi_20 = 1000023 | ~chi_30 = 1000025 |
# | ~chi_40   = 1000035 | ~chi_1+ = 1000024 | ~chi_2+ = 1000037 |
# ---------------------------------------------------------------

#particle1 = <via input line>
#particle2 = <via input line>

# Defines the computation to be performed. Three computations are supported:
# - result = total: outputs the total cross section.
# - result = pt:    outputs the value for the transverse momentum at the
#                   value specified by the `pt` variable.
# - result = ptj:    outputs the value for the transverse momentum at the
#                   value specified by the `pt` variable using the joint resummation formalism.
# - result = m:     outputs the value for the invariant mass distribution at the
#                   value specified by the `M` variable.
result = total  # total, pt, ptj or m.
M      = auto  # auto = sqrt((p1 + p2)^2)
pt     = auto

# SLHA input file that defines the SUSY benchmark point:
#slha = <via input line>
slha = %s

# PDF sets for LO and NLO. They should be present in the LHAPDF local setup.
pdf_format = lhgrid  # lhgrid or lhpdf
pdf_lo     = %s
#pdfset_lo  = <via input line>
pdf_nlo    = %s
#pdfset_nlo = <via input line>

# Factorization and renormalization scale factors. They will be multiplied by
# sum of the masses of the outgoing particles to obtain the corresponding
# scales.
# mu_f = <via input line> (1.0 nominal for Resummino v2.X)
# mu_r = <via input line> (1.0 nominal for Resummino v2.X)

# Integration parameters.
precision = 0.005 # desired precision
#max_iters = 5  # maximum iterations
max_iters = 1

# optional PDF fit parameter
# weights
# (If you get weird fit results decrease the weight up to -2.0)
#weight_valence = -1.0
#weight_sea = -1.0
#weight_gluon = -1.0
# fit PDF from xmin to 1 (auto = mis/sh)
#xmin = auto
    """ % (str(int(cme)), slha, pdf_lo, pdf_nlo))

    command = "resummino --particle1=%i --particle2=%i --pdfset_lo=%i --pdfset_nlo=%i --mu_r=%1.2f --mu_f=%1.2f --slha=%s --%s --output=%s %s" % (part1, part2, pdfset_lo, pdfset_nlo, rscale, fscale, pwd+dirname+"run/"+slha, norder, pwd+outname, pwd+resname)
    #print command, "\n"
    return command

# Generates ARTEMISA sub files, for a list of fexnames and ncores
# The .sub file is aware of its location
# TODO - Write a corret sub file: resummino is the only executable, no .sh file
# Help from https://indico.cern.ch/event/611296/contributions/2604376/attachments/1471164/2276521/TannenbaumT_UserTutorial.pdf
def genSubFile(dirname, jobs):
    fsubnames = []
    for j in jobs:
        ncores = jobs[j]["ncores"]
        fexname = jobs[j]["fexname"]
        fex = fexname[:-3]
        fsubname = fexname[:-3] + ".sub"
        flogname = fexname[:-3] + ".log"
        print "--- Creating a sub file", fsubname
        fsub = open(fsubname,"w")
        fsub.write("""\n
universe = vanilla

executable          = %s
arguments           = $(Cluster) $(Process)

log                 = %s
output              = %s$(Cluster)_$(Process).out
error               = %s$(Cluster)_$(Process).err

request_Cpus        = %s
request_Memory      = 10000

queue 1 
""" % (fexname,flogname,fex,fex,str(ncores)))        
        fsub.close()
        jobs[j].update({"fsubname": fsubname})
    return jobs

# Split list into sub-lists with n elements
def splitList(mylist, n):
    mysplitlist = []
    # For item i in a range that is a length of mylist
    for i in range(0, len(mylist), n):
        # Create an index range for l of n items:
        mysplitlist.append(mylist[i:i+n])
    return mysplitlist

# Genertes ARTEMISA submit job
# Splits commands into batches of multipes of 8 CPU cores per job
# Creates .sh executables and return a list of executable fexnames
def genExFile(dirname, commands): # .sh file needs full paths for the run location
    pwd = os.getcwd()+"/"
    
    # Split commands into batches of multipes of 8 CPU cores per sub job, 16 here
    commands_split = splitList(commands, Ncpj)

    # Prepare executable files for each set of commands, return a list of executable .sh files and ncores
    jobs = {}
    # Loop on commands_sets
    count = 0
    for commands in commands_split:
        #print commands
        ncores = len(commands)
        commands_set = ""
        for command in commands:
            commands_set = commands_set + command + "\n"

        # Prepare executable file for the condor job
        fexname = dirname + "run/resummino_" + str(count) + ".sh"
        print "--- Creating a executable file", fexname
        fex = open(fexname,"w")

        fex.write("""#!/bin/sh
echo ">>>> ENVIRONMENT"
printenv
echo ">>>> HOST"
hostname
echo ">>>> CURRENT DIR"
pwd
echo ">>>> USER"
whoami 
echo ">>>> SPACE LEFT"
df -h
echo ">>>> NVIDIA INFO"
set -x #echo on
nvidia-smi


echo "##########################################################################"
echo ">>>> RUN THE Resummino JOB"

date

source %s%srun/setup.sh

%s

ls;
pwd;

date

echo "##########################################################################"
echo ">>>> NO CLEANUP, keeping all run commands and results."

\n
    """ % (pwd,dirname,commands_set)) # Commands already have full paths
        fex.close()
        os.system("chmod u+rwx "+fexname)
        jobs[count] = {"fexname": fexname, "ncores": ncores}
        count = count + 1
    return jobs

# Generates and runs condor command
def runCondorCommand(dirname, jobs, doRun):
    count_job  = 0
    for job in jobs:
        #print "JM", jobs[job]["fsubname"], jobs[job]["fexname"]
        fsubname = jobs[job]["fsubname"]
        fexname = jobs[job]["fexname"]

        # Check if .sub and .sh files, and if run and results folder present
        try: # TODO fix this, the check is not working
            os.path.isfile(fsubname)
            os.path.isfile(fexname)
            os.path.isdir(dirname+"run/")
            os.path.isdir(dirname+"results/")
        except OSError:
            print ("ERROR: Some of the condor job structure missing at %s" % dirname)
            sys.exit()

        command = "condor_submit " + fsubname
        if not doRun: 
            print "--- Prepared condor job for Resummino"
            print "--- Submit with command:", command
        else: 
            print "--- Submitting Resummino job for", dirname
            os.system(command)
        count_job = count_job + 1
    return count_job

def genAllPDFVars(slha, prod, scheme, cme, norder, dirname):
    commands = []    

    # Produced particles - TODO: Later improve and use only PDGIDs
    # N1 = 1000022, N2 = 1000023, C1 = 1000024
    part1, part2 = 0, 0
    if prod == "N1N1":    part1, part2 = 1000022, 1000022 # Vanishing cross-section due to cancellations for pure higgsino 
    elif prod == "N1N2":  part1, part2 = 1000022, 1000023 
    elif prod == "N2N2":  part1, part2 = 1000023, 1000023 # Vanishing cross-section due to cancellations for pure higgsino 
    elif prod == "N1C1p": part1, part2 = 1000022, 1000024
    elif prod == "N1C1m": part1, part2 = 1000022, -1000024
    elif prod == "N2C1p": part1, part2 = 1000023, 1000024
    elif prod == "N2C1m": part1, part2 = 1000023, -1000024
    elif prod == "C1C1":  part1, part2 = 1000024, -1000024 
    else: 
        print "ERROR: Production", prod, "not yet implemented, exiting."
        sys.exit()

    # Initialize everything
    pdf_lo, pdfset_lo, pdf_nlo, pdfset_nlo, rscale, fscale, command = 0,0,0,0,0,0,0

    # Alpha_s variations negligible, nothing to do

    # PDF4LHC scheme cross section calculation
    # Starting point scripts from Alexaned Mann and Laura Jeanty, many thanks! 
    if scheme == "PDF4LHC":
        # Generate PDF4LHC PDF variations, PDF4LHC15_nlo_mc
        pdf_lo, pdfset_lo, pdf_nlo, pdfset_nlo, rscale, fscale, command = 0,0,0,0,0,0,0 # Clear
        # rscale=1.0, fscale=1.0  for Resummino v2.X
        rscale = 1.0
        fscale = 1.0
        # pdf_lo = MSTW, pdf_nlo = MSTW
        pdf_lo = "PDF4LHC15_nlo_mc" # No PDF4LHC at LO, use NLO
        pdf_nlo = "PDF4LHC15_nlo_mc"
        # pdfset_lo, pdfset_nlo: up(0,odd), down(0,even), (nominal 0,0 not used)
        #pdf4lhc_pdf_up = range(1,101,2) # UP 1-101 odd #pdf4lhc_pdf_do = range(2,101,2) # DOWN 2-100 even
        pdf4lhc_pdf = range(1,101,1) # UP and DOWN
        # Generate PDF4LHC variations, pdfset_lo = pdfset_nlo
        for pdfset in pdf4lhc_pdf:
            command = runResumminoIn(slha, pdf_lo, pdfset, pdf_nlo, pdfset, part1, part2, rscale, fscale, norder, cme, dirname)
            commands.append(command)

        # Generate scale variations
        pdf_lo, pdfset_lo, pdf_nlo, pdfset_nlo, rscale, fscale, command = 0,0,0,0,0,0,0 # Clear
        # rscale=[0.5, 1.0, 2.0], fscale=[0.5, 1.0, 2.0], rscale = fscale  for Resummino v2.X, separate in future!!
        scales=[0.5, 1.0, 2.0]
        # pdf_lo = MSTW, pdf_nlo = CTEQ66, MSTW
        # pdfset_lo, pdfset_nlo: 0, 0
        pdfset = 0 # pdfset_lo = pdfset_nlo
        # Generate renormalization and factorisation scales for PDF4LHC15_nlo_mc
        pdf_lo = "PDF4LHC15_nlo_mc" # No PDF4LHC at LO, use NLO
        pdf_nlo = "PDF4LHC15_nlo_mc"
        for sc in scales: # rscale = fscale
            command = runResumminoIn(slha, pdf_lo, pdfset, pdf_nlo, pdfset, part1, part2, sc, sc, norder, cme, dirname)
            commands.append(command)

    # CTEQ66 + MSTW68 cross section calculation sceeme
    # Starting points scripts from Andreas Redelbach, many thanks!
    else: # In total 44+40+6 = 90 variations
        
        # Generate CTEQ66 PDF variations
        pdf_lo, pdfset_lo, pdf_nlo, pdfset_nlo, rscale, fscale, command = 0,0,0,0,0,0,0 # Clear
        # rscale=1.0, fscale=1.0  for Resummino v2.X
        rscale = 1.0
        fscale = 1.0
        # pdf_lo = MSTWlo, pdf_nlo = CTEQ66
        pdf_lo = "MSTW2008lo68cl" # Has only 40 variations, 
        pdf_nlo = "cteq66" # Has 45 variations, but have to drop 4 and there is no cteq66 at LO
        # pdfset_lo, pdfset_nlo: up(0,odd), down(0,even), (nominal 0,0 not used)
        #cteq66_pdf_up = range(1,44,2) # UP 1-43 odd #cteq66_pdf_do = range(2,45,2) # DOWN 2-44 even
        cteq66_pdf = range(1,41,1) # UP and DOWN 44 in total, but have to use just 40 because MSTW at LO has only 40
        # Generate CTEQ66 variations 
        for pdfset in cteq66_pdf: # pdfset_lo = pdfset_nlo
            command = runResumminoIn(slha, pdf_lo, pdfset, pdf_nlo, pdfset, part1, part2, rscale, fscale, norder, cme, dirname)
            commands.append(command)

        # Generate MSTW PDF variations
        pdf_lo, pdfset_lo, pdf_nlo, pdfset_nlo, rscale, fscale, command = 0,0,0,0,0,0,0 # Clear
        # rscale=1.0, fscale=1.0  for Resummino v2.X
        rscale = 1.0
        fscale = 1.0
        # pdf_lo = MSTW, pdf_nlo = MSTW
        pdf_lo = "MSTW2008lo68cl"
        pdf_nlo = "MSTW2008nlo68cl"
        # pdfset_lo, pdfset_nlo: up(0,odd), down(0,even), (nominal 0,0 not used)
        #mstw_pdf_up = range(1,40,2) # UP 1-39 odd #mstw_pdf_do = range(2,41,2) # DOWN 2-40 even
        mstw_pdf = range(1,41,1) # UP and DOWN 40 in total
        # Generate MSTW variations 
        for pdfset in mstw_pdf: # pdfset_lo = pdfset_nlo
            command = runResumminoIn(slha, pdf_lo, pdfset, pdf_nlo, pdfset, part1, part2, rscale, fscale, norder, cme, dirname)
            commands.append(command)

        # Generate scale variations
        pdf_lo, pdfset_lo, pdf_nlo, pdfset_nlo, rscale, fscale, command = 0,0,0,0,0,0,0 # Clear
        # rscale=[0.5,1.0,2.0], fscale=[0.5,1.0,2.0], rscale = fscale  for Resummino v2.X, separate in future!!
        scales=[0.5, 1.0, 2.0] # 3 x 2 in total
        # pdf_lo = MSTW, pdf_nlo = CTEQ66, MSTW
        # pdfset_lo, pdfset_nlo: 0, 0
        pdfset_lo = 0
        pdfset_nlo = 0
        # Generate renormalization and factorisation scales for CTEQ66
        pdf_lo = "MSTW2008lo68cl"
        pdf_nlo = "cteq66"
        for sc in scales: # rscale = fscale
            command = runResumminoIn(slha, pdf_lo, pdfset_lo, pdf_nlo, pdfset_nlo, part1, part2, sc, sc, norder, cme, dirname)
            commands.append(command)
        # Generate renormalization and factorisation scales for MSTW
        pdf_lo = "MSTW2008lo68cl"
        pdf_nlo = "MSTW2008nlo68cl"
        for sc in scales: # rscale = fscale
            command = runResumminoIn(slha, pdf_lo, pdfset_lo, pdf_nlo, pdfset_nlo, part1, part2, sc, sc, norder, cme, dirname)
            commands.append(command)

    if False:
        count = 1
        for command in commands:
            print count, " ", command, "\n"
            count = count + 1

    return commands

# Create a condor one point_timestamp/production_scheme/ directory to store all input and result files
# i.e. all PDF variations for one point and production in the same folder
# All resummino.in files are in run, and output is generated to results folder
def genPointDir(slha,prod,scheme,slhadir,codepath,ts): 
    pointdirname = slha + "_" + ts + "/"
    proddirname = prod + "_" + scheme + "/"
    dirname = pointdirname + proddirname
 
    if not os.path.isdir(pointdirname):
        os.mkdir(pointdirname)
    
    try:
        os.mkdir(dirname)
        os.mkdir(dirname + "/run/") # Holds all resummino.in and input files
        os.mkdir(dirname + "/results/") # Holds all output results
        os.system("cp " + codepath + "setup.sh " + dirname + "/run/") 
        os.system("cp " + codepath + slhadir + "/" + slha + " " + dirname + "/run/") # Copy SLHA for this job
    except OSError:
        print ("ERROR: Creation of the job directory %s failed" % dirname)
        sys.exit()
    return dirname

# Prepare and run 1 point and 1 prod
def submitPointProd(slha, prod, scheme, slhadir, codepath, cme, norder, ts, doRun):
    # Create directory structure
    dirname = genPointDir(slha, prod, scheme, slhadir, codepath, ts)
 
    # Preapre all resummino.in jobs and commands for 1 point and 1 production 
    commands = genAllPDFVars(slha, prod, scheme, cme, norder, dirname)

    # Prepare .sh executable
    jobs = genExFile(dirname, commands)

    # Preapre condor .sub scripts
    jobs = genSubFile(dirname, jobs)

    # Submit condor job
    njobs = runCondorCommand(dirname, jobs, doRun)

    return njobs

# Check if the number is even or odd
def isOdd(num):
    isOdd = None
    if (num % 2) == 0: isOdd = True
    else: isOdd = False
    return isOdd

def getDSIDfromSLHA(slha):
    info = slha.split(".")
    try:
        dsid = int(info[1])
    except ValueError:
        print "WARNING: LesHouches does not have a valid DSID. Setting it to 123456."
        dsid = "123456" 
    return dsid

# Read point and production PDF and scale variations 
def readPointProdVar(slha, prod, scheme, norder, outdir, codepath):
    jobstamp = "*"
    dsid = getDSIDfromSLHA(slha)
    # Loop on all json results
    dirname = codepath + outdir + slha + "_" + jobstamp + "/" + prod + "_" + scheme + "/results/"
    if False: print "--- Reading cross-section information in", dirname
    if True: print "--- Reading cross-section information for", slha, prod, scheme
    try:
        os.path.isdir(dirname)
    except:
        print "ERROR: Wrong results path", dirname
        sys.exit()

    # List all json files and read xsections
    results = []
    for fname in glob.iglob(dirname+"/*.json"):
        #print fname 
        with open(fname) as json_file:
            try:
                data = json.load(json_file)
                results.append(data)
            except ValueError:
                print "ERROR: The json file contains a nan value. Please set by hand the missing value to the nominal.", fname
                print "Exiting."
                print "TODO: Implement reading of nan values in json file using pandas."
                sys.exit()
    #print results

    # Quickly check if all variations present
    if scheme == "CTEQ66MSTW":
        count_cteq66_pdf, count_mstw_pdf, count_cteq66_sc, count_mstw_sc = 0,0,0,0 
        for result in results:
            pdflo  = result["pdflo"]
            pdfnlo = result["pdfnlo"]
            xs     = float(result[norder])
            pdfset = int(result["pdfsetnlo"])
            mur    = str(result["mur"])
            muf    = str(result["muf"])
            
            #print pdf, xs, pdfset, mur, muf
            # CTEQ66 PDF vars
            if pdflo == "MSTW2008lo68cl" and pdfnlo == "cteq66" and mur == "1.0" and muf == "1.0" and pdfset != 0: 
                count_cteq66_pdf = count_cteq66_pdf + 1
            # MSTW PDF vars
            if pdflo == "MSTW2008lo68cl" and pdfnlo == "MSTW2008nlo68cl" and mur == "1.0" and muf == "1.0" and pdfset != 0: 
                count_mstw_pdf = count_mstw_pdf + 1
            # CTEQ66 scale vars
            if pdflo == "MSTW2008lo68cl" and pdfnlo == "cteq66" and ((mur == "0.5" and muf == "0.5") or (mur == "1.0" and muf == "1.0") or (mur == "2.0" and muf == "2.0")) and pdfset == 0: 
                count_cteq66_sc = count_cteq66_sc + 1
                #print pdflo, pdfnlo, pdfset, mur, muf, xs
            # MSTW scale vars
            if pdflo == "MSTW2008lo68cl" and pdfnlo == "MSTW2008nlo68cl" and ((mur == "0.5" and muf == "0.5") or (mur == "1.0" and muf == "1.0") or (mur == "2.0" and muf == "2.0")) and pdfset == 0: 
                count_mstw_sc = count_mstw_sc + 1
                #print pdflo, pdfnlo, pdfset, mur, muf, xs
        #print count_cteq66_pdf, count_mstw_pdf, count_cteq66_sc, count_mstw_sc
        if count_cteq66_pdf != 40 or count_mstw_pdf != 40 or count_cteq66_sc !=3 or count_mstw_sc != 3:
            print "ERROR: Wrong number of files in the results folder for PDF and scale variations found for", slha, prod
            sys.exit()
    elif scheme == "PDF4LHC": 
        count_pdf4lhc_pdf, count_pdf4lhc_sc = 0,0
        for result in results:
            pdflo  = result["pdflo"]
            pdfnlo = result["pdfnlo"]
            xs     = float(result[norder])
            pdfset = int(result["pdfsetnlo"])
            mur    = str(result["mur"])
            muf    = str(result["muf"])
            # Count PDF4LHC PDF vars
            if pdflo == "PDF4LHC15_nlo_mc" and pdfnlo == "PDF4LHC15_nlo_mc" and mur == "1.0" and muf == "1.0" and pdfset != 0:        
                count_pdf4lhc_pdf = count_pdf4lhc_pdf + 1
            # Count PDF4LHC scale vars
            if pdflo == "PDF4LHC15_nlo_mc" and pdfnlo == "PDF4LHC15_nlo_mc" and ((mur == "0.5" and muf == "0.5") or (mur == "1.0" and muf == "1.0") or (mur == "2.0" and muf == "2.0")) and pdfset == 0: 
                count_pdf4lhc_sc = count_pdf4lhc_sc + 1
        #print count_pdf4lhc_pdf, count_pdf4lhc_sc
        if count_pdf4lhc_pdf != 100 or count_pdf4lhc_sc != 3:
            print "ERROR: Wrong number of files in the results folder for PDF and scale variations found for", slha, prod
            sys.exit()
    else: print "ERROR: Not a recognized scheme", scheme

    # Calculate cross-section
    xss = {}
    if scheme == "CTEQ66MSTW":
        # Following the prescription at https://arxiv.org/abs/1206.2892
        # Read out nominal and variations cross-sections
        cteq_pdf = {} # key is set number int
        mstw_pdf = {} # key is set number int
        cteq_sc = {} # key is scale str
        mstw_sc = {} # key is scale str
        for result in results:
            pdflo  = result["pdflo"]
            pdfnlo = result["pdfnlo"]
            xs     = float(result[norder])
            pdfset = int(result["pdfsetnlo"])
            mur    = str(result["mur"])
            muf    = str(result["muf"])
            #print pdfnlo, pdfset, xs
            if pdflo == "MSTW2008lo68cl" and pdfnlo == "cteq66" and mur == "1.0" and muf == "1.0": 
                cteq_pdf[pdfset] = xs
            if pdflo == "MSTW2008lo68cl" and pdfnlo == "MSTW2008nlo68cl" and mur == "1.0" and muf == "1.0": 
                mstw_pdf[pdfset] = xs
            if pdflo == "MSTW2008lo68cl" and pdfnlo == "cteq66" and ((mur == "0.5" and muf == "0.5") or (mur == "1.0" and muf == "1.0") or (mur == "2.0" and muf == "2.0")) and pdfset == 0: 
                cteq_sc[mur] = xs
            if pdflo == "MSTW2008lo68cl" and pdfnlo == "MSTW2008nlo68cl" and ((mur == "0.5" and muf == "0.5") or (mur == "1.0" and muf == "1.0") or (mur == "2.0" and muf == "2.0")) and pdfset == 0: 
                mstw_sc[mur] = xs
        #print cteq_pdf, mstw_pdf, cteq_sc, mstw_sc
        if len(cteq_pdf) != 41 or len(mstw_pdf) != 41 or len(cteq_sc) !=3 or len(mstw_sc) != 3:
            print "ERROR: Wrong number of files in the results folder for PDF and scale variations found for", slha, prod
            sys.exit()
        if cteq_pdf[0] != cteq_sc["1.0"] or mstw_pdf[0] != mstw_sc["1.0"]:
            print "ERROR: Problem in reading out nlo cross-sections:", cteq_pdf[0], cteq_sc["1.0"], mstw_pdf[0], mstw_sc["1.0"]


        # Calculate cross-section

        # Calculate scale variations, for CTEQ66 and MSTW
        if cteq_sc["0.5"] >= 0:
            cteq_sc_err_up = cteq_sc["0.5"] - cteq_sc["1.0"] 
            cteq_sc_err_do = cteq_sc["2.0"] - cteq_sc["1.0"] 
        else:
            cteq_sc_err_up = cteq_sc["2.0"] - cteq_sc["1.0"] 
            cteq_sc_err_do = cteq_sc["0.5"] - cteq_sc["1.0"] 
        if mstw_sc["0.5"] >= 0:
            mstw_sc_err_up = mstw_sc["0.5"] - mstw_sc["1.0"] 
            mstw_sc_err_do = mstw_sc["2.0"] - mstw_sc["1.0"] 
        else:
            mstw_sc_err_up = mstw_sc["2.0"]  - mstw_sc["1.0"] 
            mstw_sc_err_do = mstw_sc["0.5"] - mstw_sc["1.0"] 
        #print "sc_err", cteq_sc_err_up, cteq_sc_err_do, mstw_sc_err_up, mstw_sc_err_do

        # Calculate PDF up and down variations, summed in quadrature, for CTEQ66 and MSTW
        # UP is odd, DOWN is even
        # CTEQ66 
        cteq_scalefactor = 1.645 # Scale from 95%CL to 68%CL for 1 sigma band
        cteq_pdf_err_up_sq, cteq_pdf_err_do_sq = 0,0
        for pdfset in cteq_pdf:
            xs = cteq_pdf[pdfset]
            if pdfset == 0: continue
            #print pdfset, isOdd(pdfset), xs
            # Add up squared odd variations for UP variation, even variations for DOWN
            delta = xs - cteq_pdf[0]
            if isOdd(pdfset): cteq_pdf_err_up_sq = cteq_pdf_err_up_sq + delta * delta
            else: cteq_pdf_err_do_sq = cteq_pdf_err_do_sq + delta * delta
        cteq_pdf_err_up = math.sqrt(cteq_pdf_err_up_sq) / cteq_scalefactor
        cteq_pdf_err_do = math.sqrt(cteq_pdf_err_do_sq) / cteq_scalefactor
        # MSTW
        mstw_scalefactor = 1.0 # MSTW at 68%CL
        mstw_pdf_err_up_sq, mstw_pdf_err_do_sq = 0,0
        for pdfset in mstw_pdf:
            xs = mstw_pdf[pdfset]
            if pdfset == 0: continue
            #print pdfset, isOdd(pdfset), xs
            # Add up squared odd variations for UP variation, even variations for DOWN
            delta = xs - mstw_pdf[0]
            if isOdd(pdfset): mstw_pdf_err_up_sq = mstw_pdf_err_up_sq + delta * delta
            else: mstw_pdf_err_do_sq = mstw_pdf_err_do_sq + delta * delta
        mstw_pdf_err_up = math.sqrt(mstw_pdf_err_up_sq) / mstw_scalefactor
        mstw_pdf_err_do = math.sqrt(mstw_pdf_err_do_sq) / mstw_scalefactor
        #print "pdf_err", cteq_pdf_err_up, cteq_pdf_err_do, mstw_pdf_err_up, mstw_pdf_err_do

        # Calculate total cross-section and uncertainty
        cteq_err_up = math.sqrt(cteq_sc_err_up*cteq_sc_err_up + cteq_pdf_err_up*cteq_pdf_err_up)
        cteq_err_do = math.sqrt(cteq_sc_err_do*cteq_sc_err_do + cteq_pdf_err_do*cteq_pdf_err_do)
        mstw_err_up = math.sqrt(mstw_sc_err_up*mstw_sc_err_up + mstw_pdf_err_up*mstw_pdf_err_up)
        mstw_err_do = math.sqrt(mstw_sc_err_do*mstw_sc_err_do + mstw_pdf_err_do*mstw_pdf_err_do)
        # Fishy, check!!
        xs_max = min(cteq_pdf[0]+cteq_err_up, mstw_pdf[0]+mstw_err_up) 
        xs_min = min(cteq_pdf[0]-cteq_err_do, mstw_pdf[0]-mstw_err_do)
        #print "JM", xs_min, xs_max
        xs = 0.5 * (xs_min + xs_max)
        xs_unc = (xs_max - xs_min) / (xs_max + xs_min) # Relative uncertainty
        xss = {"xs": xs, "xs_relunc_up": xs_unc, "xs_relunc_do": xs_unc, "fs": prod, "slha": slha, "dsid": dsid}
        #print "--- LesHouches", slha, "production", prod, xs, "+/-", xs_unc*xs, "(nominal CTEQ:", mstw_pdf[0], "MSTW", cteq_pdf[0], ")"
        #print "--- LesHouches %s production %s, %e +/- %e (nominal CTEQ: %e MSTW %e)" % (slha, prod, xs, xs_unc*xs, cteq_pdf[0], mstw_pdf[0])


    if scheme == "PDF4LHC":
        # Follwing the prescription at https://arxiv.org/pdf/1510.03865.pdf pg 50.
        pdf4lhc_pdf = {}
        pdf4lhc_sc = {}
        pdf4lhc_xslist = []
        for result in results:
            pdflo  = result["pdflo"]
            pdfnlo = result["pdfnlo"]
            xs     = float(result[norder])
            pdfset = int(result["pdfsetnlo"])
            mur    = str(result["mur"])
            muf    = str(result["muf"])
            # Count PDF4LHC PDF vars
            if pdflo == "PDF4LHC15_nlo_mc" and pdfnlo == "PDF4LHC15_nlo_mc" and mur == "1.0" and muf == "1.0":        
                if pdfset != 0: pdf4lhc_xslist.append(xs)
                pdf4lhc_pdf[pdfset] = xs
            # Count PDF4LHC scale vars
            if pdflo == "PDF4LHC15_nlo_mc" and pdfnlo == "PDF4LHC15_nlo_mc" and ((mur == "0.5" and muf == "0.5") or (mur == "1.0" and muf == "1.0") or (mur == "2.0" and muf == "2.0")) and pdfset == 0: 
                #print "JM", pdfset, xs
                pdf4lhc_sc[mur] = xs
        #print pdf4lhc_pdf, pdf4lhc_sc, pdf4lhc_xslist 
        if len(pdf4lhc_pdf) != 101 or len(pdf4lhc_sc) != 3 or len(pdf4lhc_xslist) != 100:
            print "ERROR: Wrong number of files in the results folder for PDF and scale variations found for", slha, prod 
            sys.exit()
        if pdf4lhc_pdf[0] != pdf4lhc_sc["1.0"]:
            print "ERROR: Problem in reading out nlo cross-sections:", pdf4lhc_pdf[0], pdf4lhc_sc["1.0"]

        # Calculate cross-section

        # Calculate scale uncrtainty for PDF4LHC
        if pdf4lhc_sc["1.0"] >= 0:
            pdf4lhc_sc_err_up = pdf4lhc_sc["0.5"] - pdf4lhc_sc["1.0"] 
            pdf4lhc_sc_err_do = pdf4lhc_sc["2.0"] - pdf4lhc_sc["1.0"] 
        else:
            pdf4lhc_sc_err_up = pdf4lhc_sc["2.0"] - pdf4lhc_sc["1.0"] 
            pdf4lhc_sc_err_do = pdf4lhc_sc["0.5"] - pdf4lhc_sc["1.0"] 

        # Calcualte PDF uncertainty using prescription in https://arxiv.org/pdf/1510.03865.pdf pg 50.
        # Take ascending order of cross-sections
        pdf4lhc_xslist.sort() 
        # Check if list ascending
        if False: 
            for xs in pdf4lhc_xslist: print xs
        # Calculate PDF cross-section uncertainty at 68%CL (prescription ArXiv:1510.03865), symmetric
        pdf4lhc_pdf_err_up = (pdf4lhc_xslist[84] - pdf4lhc_xslist[16]) / 2
        pdf4lhc_pdf_err_do = pdf4lhc_pdf_err_up

        # Calculate cross-section at 68%CL (prescription  ArXiv:1510.03865)
        xs_tot = (pdf4lhc_xslist[84] + pdf4lhc_xslist[16]) / 2

        # Calculate total relative cross-section uncertainty, PDF and scale uncertainty, summed in quadrature
        xs_unc_up = math.sqrt(pdf4lhc_sc_err_up*pdf4lhc_sc_err_up + pdf4lhc_pdf_err_up*pdf4lhc_pdf_err_up) / xs_tot
        xs_unc_do = math.sqrt(pdf4lhc_sc_err_do*pdf4lhc_sc_err_do + pdf4lhc_pdf_err_do*pdf4lhc_pdf_err_do) / xs_tot
        xss = {"xs": xs_tot, "xs_relunc_up": xs_unc_up, "xs_relunc_do": xs_unc_do, "fs": prod, "slha": slha, "dsid": dsid}
        if True: print "--- PDF4LHC cross-section and relative uncertainty %e, +%e, -%e" % (xs_tot, xs_unc_up, xs_unc_do)

        # Double check the cross-section uncertainty, second caluclation
        if True:
            xs_check = 0
            for xs in pdf4lhc_xslist:
                xs_check = xs_check + xs
            xs_check = xs_check / len(pdf4lhc_xslist)
            xs_pdf_err_check = 0 
            for xs in pdf4lhc_xslist:
                xs_pdf_err_check = xs_pdf_err_check + (xs-xs_check) * (xs-xs_check)
            xs_pdf_err_check = math.sqrt(xs_pdf_err_check / len(pdf4lhc_xslist)) # 1 / (N-1) is used when PDF set = 0 is used in the calcualtion, here not
            print "--- Nominal cross-section:                                  ", pdf4lhc_sc["1.0"]
            print "--- Double-check cross-section and PDF relative uncertainty:", xs_check, xs_pdf_err_check/xs_check
            print "--- Original cross-section and PDF relative uncertainty:    ", xs_tot, pdf4lhc_pdf_err_up/xs_tot, pdf4lhc_sc_err_up/xs_tot, pdf4lhc_sc_err_do/xs_tot, xs_unc_up, xs_unc_do

    return xss
        
# Make nice text file for SUSYTools cross-sections txt file - TODO
#def makeTextFile(allxss) # TODO

# Merge results and calculate all cross-sections
def getAllXS(slhas, prods, scheme, norder, outdir, codepath, outfname):
    #print "JM", slhas, prods, scheme, norder, outdir, codepath
    jobstamp = "*"
    allxss = {}
    for slha in slhas: 
        for prod in prods:
            xss = readPointProdVar(slha, prod, scheme, norder, outdir, codepath)
            allxss[slha+"_"+prod] = xss
    print "DSID, FS, xs, xs_relunc_up, xs_relunc_do #comment"
    for pp in allxss:
        print "%i, %s, %e, %e, %e #%s" % (int(allxss[pp]["dsid"]), allxss[pp]["fs"], allxss[pp]["xs"], allxss[pp]["xs_relunc_up"], allxss[pp]["xs_relunc_do"], allxss[pp]["slha"])

    with open(outfname, 'w') as outfile:
        json.dump(allxss, outfile, ensure_ascii=True, indent=4)

    print "--- Created json file with all cross-section infomration", outfname

    # Add here nice sorting and formating for the text SUSYCrossSections.txt
    #makeTextFile(allxss) # TODO
    
# Submit all condor jobs
def subAllPointProd(slhas, prods, scheme, slhadir, codepath, cme, norder, ts, doRun):
    njobs_tot = 0
    for slha in slhas: 
        for prod in prods:
            njobs = submitPointProd(slha, prod, scheme, slhadir, codepath, cme, norder, ts, doRun)
            njobs_tot = njobs_tot + njobs # Check njobs???
            if njobs_tot >= 270:
                print "WARNING: You have reached the limit of 270 on the number of condor jobs at Artemisa, not all of them are submitted. Please stop this run and re-submitt with less than 270 jobs."
    if doRun: print "--- Complete set of", njobs_tot, "Resummino condor jobs submitted for", scheme, "scheme."
    else: print "--- Complete set of", njobs_tot, "Resummino condor jobs prepared for", scheme, "scheme, not submitted."


# List all LesHouches files in slha directory
# LesHouches files always in slha folder!!!
def getSLHAs(slhadir, codepath): 
    fullslhas = glob.glob(codepath + slhadir + "/*")
    slhas = []
    # Strip full path, keep just SLHA file names
    for slha in fullslhas:
        slha = slha.split("/")[-1:][0]
        slhas.append(slha)
    #print slhas
    slhas.sort()
    return slhas

# Return a list of productions for a choice from:
# Submit a CPU/GPU job with condor
def main(argv):
    #slhadir    = "slha_bRPV_SPHENO" #"slha_bRPV" 
    slhadir    = "slha" #"slha_bRPV" 
    production = "HiggsinoInc"
    scheme     = "PDF4LHC" #"CTEQ66MSTW" 
    codepath   = os.getcwd()+"/"
    doRun      = False
    doMerge    = False
    doTest     = False 
    outdir     = ""
    cme        = 13000 # GeV
    norder     = "nll" #"nlo"
    slhas      = ["susy.449781.bRPV_mu200_tb5_MSSM.slha"]
    prods      = ["N2N2"]

    # Resummino job has: 
    # Defaults: ncores = 1, system cpu (in the sub job specifficiations)
    # Adjustable:
    # -p production process
    # -s pdf scheme for calculation: CT10+MSTW (default) or PDF4LHC
    # -c this code path 
    # -r run condor jobs
    # -o outdir directory with output results, different from ./
    # -e c.m.e
    # -n lo/nlo/nll/nllj(?)
    # -m merge resummino results, and calculate cross-section and uncertainty (default is condor job preparation)
    # -t test setup (1 point, 1 production)
    try:
        opts, args = getopt.getopt(argv,"hi:p:s:c:o:e:n:mrt",["help","inslhadir=","prod=","scheme=","codepath=","outdir=","cme=","norder="])
    except getopt.GetoptError:
        print "condor.py -i <inslhadir> -p <prod> -s <scheme> -c <codepath> -o <outdir> -e <cme> -n <norder> -m -r -t"
        sys.exit(2)
    for opt, arg in opts:
        if opt in ("-h","--help"):
            print "All LesHouches files need to be stored in the slha directory."
            print """condor.py -i <inslhadir> -p <prod> -s <scheme> -c <codepath> -o <outdir> -e <cme> -n <norder> -m -r -t
<prod> options:
Higgsino        : N1N2,N2C1p,N2C1m,C1C1
HiggsinoInc     : N1N2,N1C1p,N1C1m,N2C1p,N2C1m,N1N1,N2N2,C1C1
HiggsinoIncHigh : N1N2,N1C1p,N1C1m,N2C1p,N2C1m
HiggsinoIncLow  : N1N1,N2N2,C1C1
C1C1            : C1C1
NC              : N1N2,N2C1p,N2C1m
"""
            sys.exit()
        elif opt in ("-i", "--inslhadir"):
            slhadir = arg
        elif opt in ("-p", "--prod"):
            production = str(arg)
            if production not in allprods.keys(): # TODO - later implement grouped higgsino, EW, QCD prod and part1,part2
                print "ERROR: Production", production, "not yet implemented, exiting."
                sys.exit()
        elif opt in ("-s", "--scheme"):
            scheme = arg
            schemes = ["CTEQ66MSTW","PDF4LHC"]
            if scheme not in schemes:
                print "Not a valid scheme", scheme, "use one of", schemes
        elif opt in ("-c", "--codepath"):
            codepath = arg
        elif opt in ("-o", "--outdir"):
            outdir = arg + "/"
        elif opt in ("-e", "--cme"):
            cme = int(arg)
        elif opt in ("-n", "--norder"):
            norder = arg
        elif opt in ("-r", "--run"):
            doRun = True
            doMerge = False
        elif opt in ("-m", "--merge"):
            doMerge = True
            doRun = False
        elif opt in ("-t", "--test"):
            doTest = True
    outfname   = "ResumminoXS_"+slhadir+"_"+scheme+".json"

    #print "--- Using LesHouches:", slha, "production:", prod, "XS calcualtion scheme:", scheme, "at:", codepath
    if doRun: print "--- Submitting the job at Artemisa condor."

    # Add time-stamp for job book keeping
    ts = datetime.now().strftime("%Y-%m-%d-%H-%M-%S%z")

    # Generate all productions for all points
    if doTest:
        print "--- Using test setup for", slhas, prods, codepath
    else:
        prods = allprods[production] # Take all productions bundeled under production name
        slhas = getSLHAs(slhadir, codepath) # List all SLHA files in the slha directory
        if False: print slhadir, slhas, prods

    if doMerge:
        # Read PDF and scale variations, calculate cross-section and uncertainty
        print "Merging cross-section info for", slhas, prods, scheme
        getAllXS(slhas, prods, scheme, norder, outdir, codepath, outfname)
    else:
        # Prepare or submit all condor jobs
        print "Submitting jobs for", slhas, prods, scheme
        subAllPointProd(slhas, prods, scheme, slhadir, codepath, cme, norder, ts, doRun)



if __name__ == '__main__':
    main(sys.argv[1:]) 
