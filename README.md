# ResumminoXS

Calculate Resummino NLO+NLL cross-sections and uncertainty following the PDF4LHC15 (https://arxiv.org/abs/1510.03865, recommended) and CTEQ66 + MSTW68 prescriptions. Runs at the IFIC Artemisa condor system. The calculation validation was presetned at https://indico.cern.ch/event/883497/contributions/3725382/attachments/1976545/3293139/bRPV_ResumminoXS.pdf.

# Get and run the code

First you need to setup the dependables:

* Install GSL

Dwnload the code from https://coral.ise.lehigh.edu/jild13/2016/07/11/hello/

wget ftp://ftp.gnu.org/gnu/gsl/gsl-1.16.tar.gz

source /cvmfs/cms.cern.ch/cmsset_default.sh
export SCRAM_ARCH=slc7_amd64_gcc630
export PATH=/cvmfs/cms.cern.ch/slc7_amd64_gcc630/external/cmake/3.7.0-cms/bin/:$PATH
export PATH=/cvmfs/cms.cern.ch/slc7_amd64_gcc630/external/lhapdf/6.2.1-cms/bin/:$PATH
export PATH=/cvmfs/cms.cern.ch/slc7_amd64_gcc630/external/boost/1.63.0-cms/include/:$PATH
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/cvmfs/cms.cern.ch/slc7_amd64_gcc630/external/gcc/6.3.0-cms/lib64/

gtar -zxvf gsl-1.16.tar.gz
cd gsl-1.16
./configure --prefix=$PWD
make
make check
make install

export PATH=/lhome/ific/m/mamuzic/bRPV/Resummino/gsl-1.16/include/:$PATH

* Install and download LHAPDF

Take the code from https://lhapdf.hepforge.org/

wget http://www.hepforge.org/archive/lhapdf/LHAPDF-6.2.1.tar.gz
gtar -zxvf LHAPDF-6.2.1.tar.gz
cd LHAPDF-6.2.1
mkdir build
./configure --prefix=$PWD/build
make -j2 && make install
export PATH=/lhome/ific/m/mamuzic/bRPV/Resummino/LHAPDF-6.2.1/build/bin:$PATH
export LD_LIBRARY_PATH=/lhome/ific/m/mamuzic/bRPV/Resummino/LHAPDF-6.2.1/build/lib:$LD_LIBRARY_PATH

* Install and run Resummino
 
Take the code from https://resummino.hepforge.org/ , download Resummino 2.0.1 including LoopTools-2.13

gtar -zxvf resummino-2.0.1-looptools.tar.bz2
cd resummino-2.0.1
mkdir build
mkdir install
cd build

export LHAPATH=/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AthAnalysisExternals/21.2.39/InstallArea/x86_64-slc6-gcc62-opt/share/LHAPDF:/cvmfs/atlas.cern.ch/repo/sw/Generators/lhapdfsets/current

mkdir helpers
ln -s /usr/lib64/libbz2.so.1.0.6 libbz2.so.1.0
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/lhome/ific/m/mamuzic/bRPV/Resummino/helpers/

cmake -DCMAKE_INSTALL_PREFIX=/lhome/ific/m/mamuzic/bRPV/Resummino/resummino-2.0.1/build -DCMAKE_PREFIX_PATH=/lhome/ific/m/mamuzic/bRPV/Resummino/gsl-1.16/lib -DLHAPDF=/lhome/ific/m/mamuzic/bRPV/Resummino/LHAPDF-6.2.1/build ..

make
cd src
cat CMakeFiles/resummino.dir/link.txt > tmp.sh
source tmp.sh
cd ../bin
export PATH=/lhome/ific/m/mamuzic/bRPV/Resummino/resummino-2.0.1/build/bin:$PATH
resummino ../../input/resummino.in --lo --o proba.txt

* Get the ResumminoXS code

git clone https://gitlab.cern.ch/mamuzic/resumminoxs.git

* Run the code

The LesHouches files should be stored in the slha folder, and then you run with:

cd resumminoxs/RUN

python condor.py

* Merge the result

When the the Resummino jobs complete, merge the results with:

python condor.py -m

# TODO:

1. Add more production channels.
2. Improve user interface.
3. Add running on lxplus condor.
4. Add running on the grid.